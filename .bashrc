#
# ~/.bashrc
#

export PATH="$HOME/.local/bin:$PATH"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias ll='ls -al --color=auto'

alias pacs='doas pacman -S'
alias pacu='doas pacman -Syyu'
alias pacy='doas pacman -Syy'
alias pacr='doas pacman -R'
alias pacrrr='doas pacman -Rcns'
alias 'pac?'='pacman -Ss'



PS1=' \W > '
